#include <iostream>
#include <fstream>
#include <cstring>
#include"trie.h"
#include"mystring.h"
#include"inputobject.h"
#include"probabilitysum.h"
#include"probabilityclassifier.h"
#include"testcase.h"
#include<vector>

using namespace std;

int getFileLength(char* sourceName_){
    ifstream myfile (sourceName_,ifstream::binary);/**< read file*/
    int fileLength;

    myfile.seekg (0, myfile.end);
    fileLength = myfile.tellg();/**< determine file length*/
    myfile.close();
    return fileLength;
}

int main(int argc, char** argv) {

    int noc = 5; /**< minimum number of arguments*/

    ProbabilityClassifier* pc;
    ProbabilitySum* ps;

    vector<double> maxProb; /**< maximum probability of a model*/
    int modelNumber = 0; /**< number of corresponding model*/

    TestCase* testQueries = nullptr; /**< test cases, used if argument -t has been passed*/
    int numberOfTests = 0;

    /**< check input arguments. If less than noc show usage*/
    if (argc < noc ) {

        if (argc>1){

            string argv1 = argv[1];

            if(argv1 == "-t"){ /**< test case, compare to known results for validation*/

                /**< fill in missing arguments and pass first testcase */
                argc = 5;
                argv = new char*[5];

                argv[0] = (char*)"myPPM";
                argv[1] = (char*)"3";
                argv[2] = (char*)"3";
                argv[3] = (char*)"testCases/abc.txt";

                /**< create testcases*/
                numberOfTests = 6;
                testQueries = new TestCase[numberOfTests];

                testQueries[0] = TestCase("aa", 2, 1, 3);
                testQueries[1] = TestCase("bbb", 3, 1, 2);
                testQueries[2] = TestCase("c", 1, 0, 0);
                testQueries[3] = TestCase("abc", 3, 0, 0);
                testQueries[4] = TestCase("ab", 2, 2, 3);
                testQueries[5] = TestCase("a", 1, 3, 6);
            }

        }else{
            std::cout << "Usage:   ppmc [PPMorder] [trieOrder] [trainingData1] [trainingData2]...[trainingDataN] [testData]\n";
            std::cout << "Example: ppmc 3 5 book.txt myTestData.txt \n";
            return 0;
        }

    }

    int N = argc - noc +1 ; /**< number of training data sets*/
    Trie* trie;
    string line;
    int fileLength;
    MyString reader = MyString();

    /**< loop over number of training data sets*/
    for (int i=0;i<N;i++){

        /**< create InputObject array from source data */
        fileLength = getFileLength(argv[i+3]);
        InputObject* ms[fileLength]; /**< Array of InputObject pointers to which buffer will be mapped*/
        reader.read(argv[i+3], ms);

        /**< create empty trie*/
        trie = new Trie((int)*argv[2]-48, new MyString());

        printf("Created Trie\n");

        /**< fill trie with training data set i*/
        trie->addObjectArray(ms, fileLength);

        /**< create classifier, currently not in use*/
        ps = new ProbabilitySum(trie, (int)*argv[1]-48);
        pc = new ProbabilityClassifier(ps, 0.5);

        if (numberOfTests > 0)
            break;

        /**< read test string and initialize InputObjects*/
        if (numberOfTests == 0){
//
            fileLength = getFileLength(argv[argc-1]);
            InputObject* testIO[fileLength]; /**< Array of InputObject pointers to which buffer will be mapped*/
            reader.read(argv[argc-1], testIO);

            printf("TestString converted to InputObjects. Length: %d\n", fileLength);

            maxProb = ps->computeProbability(testIO,fileLength,'\n');

            printf("Models built. Number: %d\n", i);
            for (std::vector<double>::const_iterator i = maxProb.begin(); i != maxProb.end(); ++i)
                std::cout << *i << '\n';

            delete ps;
            delete pc;
            delete trie;
//            delete ms;
        }


    }

    if (numberOfTests ==0)
        printf("Most probable origin: data number %d, named %s\n", modelNumber, argv[3+modelNumber]);

    /**< read testcases and initialize InputObjects*/
    for (int i=0; i<numberOfTests; i++){
        testQueries[i].run(ps);
    }

    return 0;
}



#ifndef CLASSIFIER_H
#define CLASSIFIER_H
#include<model.h>

/** \brief Classifier is used to compare the probability of an input to a tolerance
* \param model model with which probability is calculated
* \param acceptanceTolerance tolerance for acceptance of this classifier
*/

class Classifier
{
    public:

        /**< constructors*/
        Classifier(Model* model_, double tol_);
        virtual ~Classifier();

        /**< components*/
        Model* model; /*model with which probability is calculated*/
        double acceptanceTolerance; /*tolerance for acceptance of this classifier*/

        /**< methods*/
        /** \brief compute probability of finding testObject in trie and return true if probability matches tolerance
        * \param testObject_ Object to test for
        * \param testObjectLength_ length of said object array
        * \return Returns true if probability computed according to model is greater than tolerance
        */
        virtual bool test(InputObject** testObject_, int testObjectLength_, char stopToken_) = 0;

    protected:

    private:
};

#endif // CLASSIFIER_H

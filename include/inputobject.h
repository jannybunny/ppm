#ifndef INPUTOBJECT_H
#define INPUTOBJECT_H

/** \brief Interface for any possible data type to classify
* \param value integer to which the object has been mapped
*/
class InputObject
{

    public:

        /**< constructors*/
        InputObject();
        virtual ~InputObject();

        /**< components*/
        int value;
        int originalLength;

        /**< methods*/

        /** \brief returns the object's alphabet size*/
        virtual int alphabetSize() = 0;

        /** \brief compares this object to object in_
        * \param in_ object to compare to
        * \return Return true if same object
        */
        virtual bool compare(InputObject* in_) = 0;

        /** \brief clone operator
        */
        virtual InputObject* clone() const = 0;

        /** \brief return this objects value*/
        virtual int getValue() = 0;

        /** \brief check if object needs to be counted in getOccurrences*/
        virtual bool isValid() = 0;

        /** \brief returns an empty object with which new objects of the initial kind can be created*/
        virtual InputObject* constructor() = 0;

        /** \brief returns an object with value val_
        * \param val_ value of returned InputObject
        * \return Return new InputObject of same class
        */
        virtual InputObject* constructor(int val_) = 0;

        /** \brief creates an InputObject array of the corresponding data type
        * \param sourceFile filename including path if necessary
        * \return InputObject array from source file
        */
        virtual void read(char* sourceName_, InputObject* ms_[]) = 0;

    protected:

    private:
};

#endif // INPUTOBJECT_H

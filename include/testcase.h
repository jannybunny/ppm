#ifndef TESTCASE_H
#define TESTCASE_H

#include"probabilitysum.h"
#include"inputobject.h"
#include"mystring.h"
#include<stdio.h>

/** \brief testcase is a simple class to check if countig and creating the trie works correctly.
* \param input This is the input as it was for better overview
* \param query The input translated into an InputObject
* \param queryLength its length
* \param expectedCount Integer for the expected amount of sightings of input in the trie
* \param expectedTotal Integer for the total amount of possibilities to continue input(1:end-1)
*/
class TestCase
{
    public:

        /**< constructors*/
        TestCase();
        TestCase(char const* in_, int length_, int expectedCount_, int expectedTotal_);
        virtual ~TestCase();

        /**< methods*/
        /** \brief Runs the test on probabilitysum ps
        * \param ps the probabilitysum used to compute probabilities
        */
        void run(ProbabilitySum* ps);

        /**< components*/
        InputObject** query;
        ProbabilitySum* ps;
        char const* input;
        int queryLength;
        int expectedCount;
        int expectedTotal;

    protected:

    private:
};

#endif // TESTCASE_H

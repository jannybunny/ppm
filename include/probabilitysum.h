#ifndef PROBABILITYSUM_H
#define PROBABILITYSUM_H

#include <Model.h>
#include <vector>

/** \brief This implementation sums up the logarithmic probabilities and returns them to the power 10*/
class ProbabilitySum : public Model
{
    public:

        /**<constructor*/
        ProbabilitySum(Trie* trie_, int order_);
        virtual ~ProbabilitySum();

        /**<components*/

        /**<methods*/
        /**< \brief compute probability of context_ in trie
        * \param context_ object pointer array to search for
        * \param contextLength length of said array*/
        vector<double> computeProbability(InputObject** context_, int contextLength_, char stopToken_);

    protected:

    private:
};

#endif // PROBABILITYSUM_H

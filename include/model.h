#ifndef MODEL_H
#define MODEL_H
#include<trie.h>
#include <vector>
using namespace std;

/** \brief Interface for a probability calculator (PPM)
* \param trie underlying trie
* \param order the PPM model order
*/
class Model
{
    public:
        /**< constructors*/
        Model(Trie* trie_, int order_);
        virtual ~Model();

        /**< components*/
        Trie* trie;
        int order;

        /**< methods*/

        /**< \brief computes probability of finding context_ in trie
        * \param context_ context to test for
        * \param contextLength_ length of said array
        * \param stopToken_ splits the context into smaller contexts separated by stopToken_
        * \return Returns probability according to used model
        */
        virtual vector<double> computeProbability(InputObject** context_, int contextLength_, char stopToken_) = 0;

    protected:

    private:
};

#endif // MODEL_H

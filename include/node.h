#ifndef NODE_H
#define NODE_H
#include"inputobject.h"
#include<vector>

/** \brief Node represents one node in the trie
* \param object InputObject of Node
* \param parentContext counts how often parent has been used in a context
* \param usedContext counts how often this node has been used as context
* \param numberOfChildren counts children (deprecated)
* \param parent pointer to parent node
* \param children array of children
* \param right pointer to the right node on the same level as this node (deprecated)
* \param childrenIndex vector with indexes where children are stored
*/
class Node
{
    public:

        /**< constructors */
        /** \brief create empty node
        */
        Node();

        /** \brief create node with an object an parent, but otherwise NULL
        * \param object_ InputObject to initialize with
        * \param parent parent node of node
        */
        Node(InputObject* object_, Node* parent_);

        /** \brief create fully functioning node
        * \param object_ InputObject to initialize with
        * \param parent parent node of node
        * \param usedContext_ counter how often node has been used a s context
        * \param parentContext counter how often parent has been used as context
        */
        Node(InputObject* object_, int parentContext_, int usedContext_, Node* parent_);

        virtual ~Node();

        /**< components*/
        InputObject* object; /**< InputObject of Node */
        int parentContext; /**< counts how often parent has been used in a context*/
        int usedContext; /**< counts how often this node has been used as context*/
        int numberOfChildren; /**< counts children (deprecated)*/
        Node* parent; /**< pointer to parent node*/
        Node** children; /**< array of children*/
        Node* right; /**< pointer to the right node on the same level as this node (deprecated)*/
        std::vector<int> childrenIndex; /**< vector with indexes where children are stored*/

        /**< methods */
        /** \brief get Child of node with the same value as in_
        * \param in_ InputObject to compare to
        * \return Returns child with the same value as in_ or InputObject with value -1
        */
        Node* getChild(InputObject* in_);
    protected:


    private:
};

#endif // NODE_H

#ifndef TRIE_H
#define TRIE_H
#include"inputobject.h"
#include"node.h"

/**\brief  This class creates a trie to use for PPM classification
* \param occurences how often a target came after context
* \param total count of other objects that came after context
* \param root top node without parent
* \param depth depth of trie, i.e. maximum allowed levels excluding root
* \param singleObjectCounter deprecated
*/
class Trie
{
    public:
        /**< constructors*/
        Trie();
        Trie(int depth_, InputObject* object_);
        virtual ~Trie();

        /**< components*/
        Node* root; /**< top node without parent*/
        int depth; /**< depth of trie, i.e. maximum allowed levels excluding root*/
        int* singleObjectCounter; /**/

        int occurences; /**< how often a target came after context*/
        int total; /**< count of other objects that came after context*/

        /**< methods*/

        /**< \brief adds an array of inputobjects to the trie and counts occurrences
        * \param in_ array of inputoObject pointers added to the trie
        * \param inSize_ size of said array
        * \return Returns true if everything worked
        */
        bool addObjectArray(InputObject** in_, int inSize_);

        /** \brief writes the number of occurrences of context_ into trie->occurrences and the number of all possibilities in trie->total
        * \param context_ total context converted to InputObject array*
        * \param contextLength_ length of input array
        * \param currentContext_ each time context_[0] is spotted, this method searches for context_[1] next which is currentContext_[0] and so on
                            currentContext_ is the context the method currently searches for
        * \param currentLength_ length of the current context
        * \param parent_ parent node at which we start looking
        */
        void getAllOccurences(InputObject** context_, int contextLength_, InputObject** currentContext_, int currentLength_, Node* parent_);


    protected:

    private:
};

#endif // TRIE_H

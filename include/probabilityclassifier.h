#ifndef PROBABILITYCLASSIFIER_H
#define PROBABILITYCLASSIFIER_H

#include <Classifier.h>

/** \brief This implementation simply compares the probability to a tolerance*/
class ProbabilityClassifier : public Classifier
{
    public:

        /**< constructor*/
        ProbabilityClassifier(Model* model_, double tol_);
        virtual ~ProbabilityClassifier();

        /**< components*/


        /**< methods*/
        bool test(InputObject** testObject_, int testObjectLength_, char stopToken_);

    protected:

    private:
};

#endif // PROBABILITYCLASSIFIER_H

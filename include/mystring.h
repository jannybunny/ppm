#ifndef MYSTRING_H
#define MYSTRING_H
#include "inputobject.h"

/** \brief one implementation of InputObject, MyString creates a string object to classify*/
class MyString: public InputObject
{

    public:

        /**< constructors*/
        MyString(int val_);
        MyString(){value = -1;};
        virtual ~MyString();
        MyString(const MyString&){};

        /**< components*/
        int value;

        /**< methods*/
        MyString* clone() const { return new MyString(this->value); };

        int alphabetSize();
        bool compare(InputObject* ms_);
        int getValue();
        bool isValid();
        MyString* constructor();
        MyString* constructor(int val_);
        void read(char* sourceName_, InputObject* ms_[]);

    protected:

    private:
};

#endif // MYSTRING_H

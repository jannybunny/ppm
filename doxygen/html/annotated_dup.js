var annotated_dup =
[
    [ "Classifier", "class_classifier.html", "class_classifier" ],
    [ "InputObject", "class_input_object.html", "class_input_object" ],
    [ "Model", "class_model.html", "class_model" ],
    [ "MyString", "class_my_string.html", "class_my_string" ],
    [ "Node", "class_node.html", "class_node" ],
    [ "ProbabilityClassifier", "class_probability_classifier.html", "class_probability_classifier" ],
    [ "ProbabilitySum", "class_probability_sum.html", "class_probability_sum" ],
    [ "Trie", "class_trie.html", "class_trie" ]
];
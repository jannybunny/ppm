var class_my_string =
[
    [ "MyString", "class_my_string.html#a083a397d42b764da08804fcf7200192b", null ],
    [ "MyString", "class_my_string.html#a1cb17852b83614394b59720779c5f918", null ],
    [ "~MyString", "class_my_string.html#a7bee4fe8ad82a0b7b8f65b02054b156b", null ],
    [ "MyString", "class_my_string.html#ad7f80e6d17c22668b65a4b93e386133c", null ],
    [ "alphabetSize", "class_my_string.html#ac8ff985c7d3b5e726b637c161d81f2c4", null ],
    [ "clone", "class_my_string.html#a731f1e50df6f84c51c2cfc8e1b4ccd04", null ],
    [ "compare", "class_my_string.html#aa6e2b9544e3ec6b1f67e56b96f6b8d9b", null ],
    [ "constructor", "class_my_string.html#a88d35823f3192b43f51fbff56d0050a5", null ],
    [ "constructor", "class_my_string.html#af9a6ce17d538d981d7e3fa9bc97e7b70", null ],
    [ "getValue", "class_my_string.html#aafc51429c085fc7cecc516a94d214014", null ],
    [ "isValid", "class_my_string.html#a975224c0ac3fe467be74b5c2236b5cda", null ],
    [ "value", "class_my_string.html#a7464becf8bd905ea19912486067c1c8d", null ]
];
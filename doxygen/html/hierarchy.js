var hierarchy =
[
    [ "Classifier", "class_classifier.html", [
      [ "ProbabilityClassifier", "class_probability_classifier.html", null ]
    ] ],
    [ "InputObject", "class_input_object.html", [
      [ "MyString", "class_my_string.html", null ]
    ] ],
    [ "Model", "class_model.html", [
      [ "ProbabilitySum", "class_probability_sum.html", null ]
    ] ],
    [ "Node", "class_node.html", null ],
    [ "Trie", "class_trie.html", null ]
];
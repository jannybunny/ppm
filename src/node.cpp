#include "node.h"
#include<stdio.h>
#include<iostream>

Node::Node()
{
    object = NULL;
    parent = NULL;
    children = NULL;
    usedContext = 0;
    parentContext = 0;
    numberOfChildren = 0;
    right = 0;
}

Node::Node(InputObject* object_, Node* parent_)
{
    object = object_;
    parent = parent_;
    children = NULL;
    usedContext = 0;
    parentContext = 0;
    numberOfChildren = 0;
    right = 0;
}

Node::Node(InputObject* object_, int parentContext_, int usedContext_, Node* parent_)
{
    object = object_;
    parentContext = parentContext_;
    usedContext = usedContext_;
    parent = parent_;
    children = new Node*[object_->alphabetSize()];
    children[0] = new Node(object_->constructor(), parent_);

    for(int i=0;i<object->alphabetSize()-1;i++){
        children[i+1] = new Node(object_->constructor(), parent_);
        children[i]->right = children[i+1];
    }

    numberOfChildren = 0;
    right = 0;
}

Node* Node::getChild(InputObject* in_){
    for (int i = 0; i< in_->alphabetSize(); i++){

        if (children[i]->object->getValue()>=0){
            if (children[i]->object->compare(in_)){
                return children[i];
            }
        }
    }
    return new Node(in_->constructor(), this);
}



Node::~Node()
{
    //dtor
}

#include "mystring.h"
#include "inputobject.h"
#include <fstream>
#include<stdio.h>

using namespace std;

MyString::MyString(int val_): InputObject()
{
    value = val_;
}

MyString::~MyString()
{
}

MyString* MyString::constructor(){
    return new MyString();
}

MyString* MyString::constructor(int val_){
    return new MyString(val_);
}

bool MyString::compare(InputObject* ms_){
    return value == ms_->getValue();
}

int MyString::getValue(){
    return value;
}

bool MyString::isValid(){
    return value > 0;
}

/**< for a string the alphabet contains 256 possible inputs*/
int MyString::alphabetSize(){
    return 256;
}

 void MyString::read(char* sourceName_, InputObject* ms_[]){

    ifstream myfile (sourceName_,ifstream::binary);/**< read file*/
    int fileLength;
    char * buffer;

    myfile.seekg (0, myfile.end);
    fileLength = myfile.tellg();/**< determine file length*/
    myfile.seekg (0, myfile.beg);/**< reset pointer to beginning of file*/

    if (fileLength<0){
        printf("FILE_NOT_EXISTING_ERROR: THE FILE YOU HAVE SPECIFIED DOES NOT EXIST! CHECK THE PATH!\n");
        return;
    }

    buffer = new char [fileLength];/**< initialize text buffer*/
    myfile.read (buffer,fileLength);

//    InputObject* ms[fileLength]; /**< Array of InputObject pointers to which buffer will be mapped*/

    /**< Read buffer and fill said array*/
    if (myfile.is_open()){

        for(int h=0; h< fileLength;h++){
            ms_[h] = new MyString(buffer[h]);
        }

        myfile.close();
    }

    return;

}

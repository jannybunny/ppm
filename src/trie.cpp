#include "trie.h"
#include"stdio.h"
#include<iostream>

Trie::Trie(int depth_, InputObject* object_)
{
    depth = depth_;
    root = new Node(object_, 0, 0, 0);
    singleObjectCounter = new int[object_->alphabetSize()];
    occurences = 0;
    total = 0;
}

Trie::~Trie()
{
    //dtor
}



bool Trie::addObjectArray(InputObject** in_, int inSize_){
    Node* child; /**< always start at root*/
    Node* parent;

    /**< only allow training sets with greater size than depth*/
    if (inSize_<depth){
        printf("Training set must be greater than trie depth!");
        return false;
    }

    for (int i=0; i < inSize_-depth+1; i++){

        for (int j=1; j <= depth; j++){
            /**< always start at root node*/
            child = root;

            /**< add Values*/
            for(int k = i; k<i+j; k++){

                /**< do not store invalid objects*/
                if (!in_[k]->isValid())
                    break;

                parent = child;
                singleObjectCounter[in_[k]->getValue()]++;

                /**< if parent has no children*/
                if (parent->numberOfChildren==0){
                    child = new Node(in_[k],parent->usedContext,1,parent);
                    parent->children[in_[k]->getValue()] = child;
                    parent->childrenIndex.push_back(in_[k]->getValue());
                    parent->numberOfChildren++;
                }else{
                    child = parent->getChild(in_[k]);

                    /**< if parent has children but none fits (catch null)*/
                    if(child->object->getValue()<0){
                        child = new Node(in_[k],parent->usedContext,1, parent);
                        parent->children[in_[k]->getValue()] = child;
                        parent->childrenIndex.push_back(in_[k]->getValue());
                        parent->numberOfChildren++;
                    }else{

                    /**< increase counter*/
                        if (k == i+j-1){
                            child->parentContext++;
                            child->usedContext++;
                        }
                    }
                }

            }

        }

    }

    return true;

}


void Trie::getAllOccurences(InputObject** context_, int contextLength_, InputObject** currentContext_, int currentLength_, Node* currentNode_){

    InputObject** nextObject = new InputObject*;

    Node* parent;

    int nextLength;

    /**< if current node matches currentContext_[0]*/
    if(currentNode_->object->compare(*currentContext_)){

        if (currentLength_==2 && currentNode_->childrenIndex.size()>0){/**< end of contextObject, count occurrences*/

            /**< occurrences = 0 if not found, but total is returned for escape probability*/
            occurences += currentNode_->children[currentContext_[1]->getValue()]->usedContext;

            /**< count total possibilities*/
            for (size_t j = 0;j<currentNode_->childrenIndex.size();j++){
                total += currentNode_->children[currentNode_->childrenIndex.at(j)]->usedContext;
            }

            return;

        }else{

            if(currentLength_ == 1 && currentNode_->childrenIndex.size()>0){ /**< one object only*/
                occurences += currentNode_->usedContext;
                parent = currentNode_->parent;

                /**< count total possibilities*/
                for (size_t j = 0;j<parent->childrenIndex.size();j++){
                    total += parent->children[parent->childrenIndex.at(j)]->usedContext;
                }

                return;

            }else{ /**< continue with partial context*/
                nextObject = currentContext_;
                nextObject++;
                nextLength = currentLength_-1;
            }
        }

    }else{/**< no match*/

        /**< only continue if current node is not root*/
        if (currentNode_->parent)
            return;

        nextObject = context_;
        nextLength = contextLength_;
    }

    /**< check all children for occurrences*/
    for (size_t i=0;i<currentNode_->childrenIndex.size();i++){
        getAllOccurences(context_, contextLength_, nextObject, nextLength, currentNode_->children[currentNode_->childrenIndex[i]]);
    }

    return;

}


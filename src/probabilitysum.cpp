#include "probabilitysum.h"
#include<stdio.h>
#include <math.h>
#include <vector>

using namespace std;

ProbabilitySum::ProbabilitySum(Trie* trie_, int order_): Model(trie_, order_)
{
}

ProbabilitySum::~ProbabilitySum()
{
}

vector<double> ProbabilitySum::computeProbability(InputObject** context_, int contextLength_, char stopToken_){

    vector<double> result;

    int iteratorEnd, contextEnd = 0, lastContextEnd, actualEnd, length2nextStopToken, localCount;

    bool done = false;

    /**< context to check*/
    InputObject** currentContext;

    /**< depending on model order set length of context array to test*/
    if (order < contextLength_){
        iteratorEnd = order;
    }else{
        iteratorEnd = contextLength_;
    }

    /**< while end of file not reached */
    while(!done){

        /**< initialize probability*/
        double prob = 0;

        /**< Only go through test array once if shorter than model order*/
        if (contextLength_<=order){
            contextEnd = 1;
            done = true;
        }else{

            if (stopToken_){
                /**< get length of context to next stopToken_*/
                localCount = contextEnd+order;
                lastContextEnd = contextEnd+order;

                while(localCount<contextLength_){
                    if(context_[localCount]->compare(context_[0]->constructor(stopToken_))){
                        contextEnd = localCount;
                        break;
                    }
                    localCount++;
                }

                length2nextStopToken = contextEnd-lastContextEnd;

                if (length2nextStopToken<=order){
                    contextEnd = lastContextEnd+1;
                }else{
                    contextEnd = contextEnd-order+1;
                }

            }else{
                contextEnd = contextLength_-order+1;
            }
        }

        if (contextEnd>contextLength_-order+1 && contextEnd!=1) /** TODO!!! DOES NOT WORK FOR SMALL CONTEXTS*/
            break;

        for(int j=lastContextEnd;j<contextEnd;j++){

            /**<go through context. for example (model order > 4) hello -> hello, ello, llo, lo, o,...*/
            for(int i=0; i<iteratorEnd; i++){

                actualEnd = iteratorEnd-i;

                currentContext = new InputObject*[actualEnd];

                /**< create array with current context. Break on invalid character*/
                for (int k=i;k<iteratorEnd;k++){

                     if ( !context_[k+j]->isValid() ){
                        actualEnd = k;
                        break;
                     }

                     printf("%c", context_[k+j]->getValue());
                     currentContext[k-i] = context_[k+j]->constructor(context_[k+j]->getValue());
                }
                printf("\n");

                /**< if first object was invalid, stop*/
                if (actualEnd==0)
                    break;

                /**< reset counter*/
                trie->occurences = 0;
                trie->total = 0;
                trie->getAllOccurences(currentContext, actualEnd, currentContext, actualEnd, trie->root);

                printf("Total Count: %d %d\n", trie->occurences, trie->total);

                /**< If match has been found, return*/
                if (trie->occurences>0){
                    printf("Probability calculates to %f\n", prob+log10( (double)trie->occurences/ ( trie->total + 1 )));
                    prob += log10((double)trie->occurences/ ( trie->total + 1 ));
                    break;
                }else{

                    if (trie->total > 0){
                        prob += log10((double)1/ ( trie->total + 1 ));
                    }else{
                        /**< if no match found, decrease probability before restarting with less context (model order) if size allows that*/
                        if ( iteratorEnd-i>1 ){
                            printf("Object not found. Reducing length by one.\n");
                            prob += log10((double)1/ currentContext[0]->alphabetSize());
                        }
                        else{ /**< order minus one model*/
                            prob += log10((double)1 / currentContext[0]->alphabetSize());

                            printf("Order minus one: Probability calculates to %f\n", prob);
                            break;
                        }
                    }

                }

            }

        }

        result.push_back(pow(10,prob));
    }

//    printf("Probability calculates to %f\n",  pow(10,prob));
    return result;

}

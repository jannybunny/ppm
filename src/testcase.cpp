#include "testcase.h"

using namespace std;


TestCase::TestCase(char const* in_, int length_, int expectedCount_, int expectedTotal_)
{
    input = in_;
    query = new InputObject*[length_];

    for(int j=0;j<length_;j++){
        query[j] = new MyString(in_[j]);
    }

    queryLength = length_;
    expectedCount = expectedCount_;
    expectedTotal = expectedTotal_;
}

TestCase::TestCase(){

}

TestCase::~TestCase()
{
    //dtor
}

void TestCase::run(ProbabilitySum* ps_){
    ps = ps_;

    printf("Case %s. \nProbability: %f \n", input,ps->computeProbability(query,queryLength, 0));
    printf("Count: %d \nTotal Possibilities: %d\n\n", ps->trie->occurences, ps->trie->total);

    if (ps->trie->occurences != expectedCount){
        printf("OCCURRENCES_COUNT_ERROR: DIFFERENCE BETWEEN EXPECTED AND REAL COUNT OF SEARCH RESULTS!\n");
        return;
    }

    if(ps->trie->total != expectedTotal){
        printf("TOTAL_COUNT_ERROR: DIFFERENCE BETWEEN EXPECTED AND REAL TOTAL OF SEARCH RESULTS!\n");
        return;
    }

    printf("SUCCESS!\n");

}
